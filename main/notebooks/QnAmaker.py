#!/usr/bin/env python
# coding: utf-8

# One time executions

# In[1]:


import nltk
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
nltk.download('universal_tagset')
get_ipython().system('wget http://setup.johnsnowlabs.com/colab.sh -O - | bash')


# In[2]:


# Suppress Warnings
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
warnings.filterwarnings('ignore')
warnings.filterwarnings('ignore', category=DeprecationWarning)


# In[3]:


import pandas as pd
import numpy as np
import nltk
import re
import csv
import string
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score

from nltk.tokenize import word_tokenize
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import seaborn as sns


# In[4]:


def removeFeaturesHavingNoValues(df):
    missing_data = df.isnull()
    df.drop(['uniq_id', 'crawl_timestamp'], axis = 1,inplace = True)
    for column in missing_data.columns.values.tolist():
        if missing_data[column].value_counts().get(True) == df.shape[0]:
            print("removing ", column)
            df.drop([column], axis = 1,inplace = True)


# In[5]:


df_job=pd.read_csv('job posting dataset.csv')


# In[6]:


# Printing shape of dataset
df_job.shape


# In[7]:


# printing Columns
df_job.columns


# In[8]:


df_job.describe()


# In[9]:


# Priniting top 5 rows
df_job.head()


# **It is evident from dataset that columns, 'uniq_id', 'crawl_timestamp' can be ignored. We can categorize remaining columns as relevant and irrelevant data**
# 
# **Relevant data** : 'job_title', 'job_description', 'job_type', 'categories', 'companydescription'
#       
#        
# **Irrelevant Data** : 'location',
#        'city', 'state', 'country', 'zip_code', 'address', 'salary_from',
#        'salary_to', 'salary_period', 'apply_url', 'apply_email', 'employees',
#        'industry', 'company_name', 'employer_email', 'employer_website',
#        'employer_phone', 'employer_logo', 
#        'employer_location', 'employer_city', 'employer_state',
#        'employer_country', 'employer_zip_code', 'uniq_id', 'crawl_timestamp'
# 

# In[10]:


## Creating a copy of dataframe
df_mod=df_job.copy()
## Calling function to remove features having no values
removeFeaturesHavingNoValues(df_mod)


# In[11]:


df_mod


# In[12]:


# printing Columns
df_mod.columns


# In[13]:


# Printing shape of dataset
df_mod.shape


# In[14]:


# Priniting top 5 rows
df_mod.head()


# ## It is evident from the dataframe that columns, 'uniq_id', 'crawl_timestamp' can be ignored.
# 
# ## Columns like (location and employer_location),(city & employer_city), (state & employer_state), (country & employer_country)
# ## ,(zip_code & employer_zip_code) are duplicates. If non- duplicates, employer details are null values.
# 
# ## Also location is combination of ('city', 'state', 'zip_code') columns . So we can consider one among them, preferably combination will work.
# 
# ## Also employer_location is combination of ('employer_city', 'employer_state', 'employer_zip_code') columns . So we can consider one among them, preferably combination will work.
# 
# ## Following columns/features has been removed as there were no values associated with them.
#   ('job_type', 'categories','address', 'salary_from', 'salary_to', 'salary_period', 'apply_url', 'apply_email', 'employees', 'industry', 'company_name', 'employer_email', 'employer_website', 'employer_phone')

# ## Creating a new dataframe

# In[15]:


df_base = df_mod[['job_title', 'job_description', 'location', 'country', 'apply_url', 'company_name', 'employer_logo','companydescription', 'employer_location']]


# In[16]:


df_base.head()


# In[17]:


df_base.groupby(['job_title'])['location'].count()


# In[18]:


# Unique values in Job Title
df_base['job_title'].unique()


# In[19]:


# Unique values in Job Title
df_base.groupby(['job_title'])['job_title'].count()


# In[20]:


# Getting values for each job title
df_base.job_title.value_counts()


# ## Account Executive  & Sales Associate/Beauty Advisor  are the most indemand job.

# In[21]:


df_base.groupby(['job_title']).count()


# Functions

# In[22]:


from nltk.corpus import stopwords
#StopWord Removal 
stop_words = set(stopwords.words('english'))
def remove_stop_words(sentence):
    word_tokens = word_tokenize(sentence)
    
    filtered_sentence = []
    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)
            
    return " ".join(filtered_sentence)

# Punctuation Removal
symbols = "!\"#$%&()*+-./:;<=>?@[\]^_`{|}~\n"
def remove_punctuation(sentence):
    word_tokens = word_tokenize(sentence)
    
    filtered_sentence = []
    for w in word_tokens:
        if w not in symbols:
            filtered_sentence.append(w)
            
    return " ".join(filtered_sentence)

def remove_special_characters(text):
    pattern = r'[^a-zA-z0-9\s]'
    text = re.sub(pattern, '', text)
    text = text.lower()
    return text

def basic_preprocess_text(text):

    #Remove stopWords
    text = remove_stop_words(text)
    
    #Remove punctutation
#     text = remove_punctuation(text)
    
    #Removal special characters
#     text = re.sub("(\\W)+"," ",text)
    text = re.sub("\n+", ". ", text)
    return text


# In[23]:


from nltk.stem.porter import PorterStemmer
porter_stemmer  = PorterStemmer()


# In[24]:


skills_keywords = ['skills', 'knowledge', 'Abilities', 'JOB SPECIFICATIONS']
roles_responsibility_keywords = ['responsibilities', 'job functions', 'duties', 'Requirements', 'What Youll Need']

stemmed_skills_keywords = [porter_stemmer.stem(word) for word in skills_keywords]
stemmed_roles_responsibility_keywords = [porter_stemmer.stem(word) for word in roles_responsibility_keywords]

def can_we_consider_this_section_header(section_header):
        if any(s in section_header.lower() for s in stemmed_skills_keywords):
            return {'consider' : True, 'category' : 'Skills'}
        if any(s in section_header.lower() for s in stemmed_roles_responsibility_keywords):
            return {'consider' : True, 'category' : 'Responsibility'}
        return {'consider' : False, 'category' : 'Other'}


# In[25]:


sections_list = [];
def extract_sections(df, idx, sections_list):
    soup = BeautifulSoup(df_job['job_description'][idx], 'lxml')
    
    job_description = soup.findAll('div',{'class':'jobsearch-jobDescriptionText'})[0].text
    all_section_headers = soup.find_all('b')
    
    for i in range (0,len(all_section_headers)):
        section_head = {}
        
        section_consideration_result = can_we_consider_this_section_header(all_section_headers[i].text)
        if not section_consideration_result['consider']:
            continue;
        
        section_head['category'] = section_consideration_result['category']
        section_head['title'] = all_section_headers[i].text
        
        start_idx = job_description.index(all_section_headers[i].text) + len(all_section_headers[i].text)
        end_idx = len(job_description) if len(all_section_headers) == i+1 else job_description.index(all_section_headers[i+1].text)
        
        content = job_description[start_idx:end_idx]
        content = re.sub(r'\n+', '. ', content).strip()
        section_head['content'] = content
        
        sections_list.append(section_head)


# In[26]:


def extract_section(content , sections_list):
    soup = BeautifulSoup(content, 'lxml')
    
    job_description = soup.findAll('div',{'class':'jobsearch-jobDescriptionText'})[0].text
    all_section_headers = soup.find_all('b')
    
    for i in range (0,len(all_section_headers)):
        section_head = {}
        
        section_consideration_result = can_we_consider_this_section_header(all_section_headers[i].text)
        if not section_consideration_result['consider']:
            continue;
        
        section_head['category'] = section_consideration_result['category']
        section_head['title'] = all_section_headers[i].text
        
        start_idx = job_description.index(all_section_headers[i].text) + len(all_section_headers[i].text)
        end_idx = len(job_description) if len(all_section_headers) == i+1 else job_description.index(all_section_headers[i+1].text)
        
        content = job_description[start_idx:end_idx]
        content = re.sub(r'\n+', '. ', content).strip()
        section_head['content'] = content
        
        sections_list.append(section_head)


# In[27]:


for i in range(1,3):
    extract_sections(df_job,i,sections_list)
df_sections = pd.DataFrame(sections_list)
df_sections.head(100)

df_sections.to_csv('section_wise_extracted.csv', index=False)
# In[28]:


import sparknlp
from sparknlp.annotator import *
from sparknlp.base import *


# In[29]:


# if you have GPU
# spark = sparknlp.start(gpu=True)
spark = sparknlp.start()

print("Spark NLP version: ", sparknlp.version())
print("Apache Spark version: ", spark.version)


# In[30]:


import json
import os
from pyspark.ml import Pipeline
from sparknlp.base import *
from sparknlp.annotator import *
import sparknlp

spark = sparknlp.start()

def get_ann_pipeline ():
    
    document_assembler = DocumentAssembler()         .setInputCol("text")        .setOutputCol('document')

    sentence = SentenceDetector()        .setInputCols(['document'])        .setOutputCol('sentence')        .setCustomBounds(['\n'])

    tokenizer = Tokenizer()         .setInputCols(["sentence"])         .setOutputCol("token")

    pos = PerceptronModel.pretrained()               .setInputCols(["sentence", "token"])               .setOutputCol("pos")
    
    embeddings = WordEmbeddingsModel.pretrained()          .setInputCols(["sentence", "token"])          .setOutputCol("embeddings")

    ner_model = NerDLModel.pretrained()           .setInputCols(["sentence", "token", "embeddings"])           .setOutputCol("ner")

    ner_converter = NerConverter()      .setInputCols(["sentence", "token", "ner"])      .setOutputCol("ner_chunk")

    ner_pipeline = Pipeline(
        stages = [
            document_assembler,
            sentence,
            tokenizer,
            pos,
            embeddings,
            ner_model,
            ner_converter
        ]
    )

    empty_data = spark.createDataFrame([[""]]).toDF("text")

    ner_pipelineFit = ner_pipeline.fit(empty_data)

    ner_lp_pipeline = LightPipeline(ner_pipelineFit)

    print ("Spark NLP NER lightpipeline is created")

    return ner_lp_pipeline


# In[31]:


conll_pipeline = get_ann_pipeline ()


# In[32]:


grammar = "NN: {<NNP>+|<NN>+}"
cp = nltk.RegexpParser(grammar)
def extract_nouns_as_skills(content):
    skills = []
    for sent in sent_tokenize(content):
        tokens = nltk.word_tokenize(sent)
        tag = nltk.pos_tag(tokens)
        result = cp.parse(tag)
        extract_nouns(result, skills)
    return skills

def extract_nouns(pos_tagged_result, skills):
    for tag in pos_tagged_result:
        if type(tag) is tuple and (tag[1] == 'NNP' or tag[1] == 'NN') and len(tag[0]) > 1 and tag[0] not in all_skills:
            skills.append(tag[0])
        elif type(tag) is nltk.tree.Tree:
            skill = ''
            for t in tag:
                if type(t) is tuple and (t[1] == 'NNP' or t[1] == 'NN') and len(t[0]) > 1:
                    skill = skill +' '+ t[0]
            if len(skill) > 1 and skill.strip() not in all_skills:
                skills.append(skill.strip())


# In[33]:


# def extranct_skills_from_content():
#     text = df_skills.content.values[i]
#     skills = extract_nouns_as_skills(text)
#     print(skills)


# In[34]:


def extract_intent_skills_based_on_category(row):
    if row['category'] == 'Skills':
        return extract_skills(row['content'])
    elif row['category'] == 'Responsibility':
        return extract_intent(row['content'])


# In[35]:


def extract_intent(content):
    intent_text = ''
    parsed = conll_pipeline.annotate (content)
    for token, pos, ner in zip(parsed['token'],parsed['pos'],parsed['ner']):
        if (((pos =='NP') or (pos =='NNP') or (pos[0] =='V') ) or (ner != 'O')): 
            intent_text = intent_text  + token + " "
    return intent_text


# In[36]:


from nltk.tokenize import word_tokenize
#Dictionary for programming language 
all_skills = [ 'Word', 'Excel', 'Outlook', 'decision making','POS','customer environment',
             'Microsoft Office', 'PowerPoint', 'publishing software', 'Adobe', 'Publisher','Computer Skills',
             'GED','math','revenue recognition accounting ..','cost accounting', 'EAC', 'ETC', 
              'software engineering experience', 'Golang', 'Java', 'Python', 'Javascript','stack development',
              'Adobe Experience Manager','Adobe Marketing Cloud','MS Excel', 'Access', 'Office',
              'PIVOTS', 'VLOOKUPs','eCommerce','SAP', 'Certified AS9100',
              'aircraft delivery', 'Lockheed Martin Aero Fighter','CRM/Marketing Automation Platform ..',
              'Business Administration', 'Logistics', 'Supply Chain','Medical', 'dental', 'vision', 'insurance',
              'Opportunity Affirmative Action', 'Veterans Disabled Individuals Employer',
              'infrastructure construction', 'manufacturing', 'product', 'Demonstrate', 'business', 'marketing', 
              'infrastructure construction market', 'future industry', 'software','GateHouse Media Management',
              'database', 'business marketing', 'RFP', 'development','market/business',
              'environment', 'Proficiency', 'emphasis', 'communication', 'Attention', 'work ethic','Power Point',
              'engineering production','python', 'c','r', 'c++','Java','hadoop','scala','flask','pandas',
              'spark','scikit-learn','Golang','numpy','php','sql','mysql','css','mongdb','nltk','fastai' ,
              'keras', 'pytorch','tensorflow','linux','Ruby','JavaScript','django','react','reactjs','ai',
              'ui','tableau','Scikit-Learn' , 'Keras', 'Theano', 'SciPy', 'Selenium', 'Robot Framework', 
              'TestComplete','Beautiful Soup', 'LXML', 'Scrapy', 'Urllib','banking','lending/borrowing process',
              'SFDC', 'ETL Tools','CRM', 'Marketing','AGILE/SCRUM Processes','inventory management', 'merchandising',
              'management', 'interaction', 'information', 'analyze', 'conflict', 'demonstrate reasoning', 
              'adaptability','conceptualizing', 'attention', 'time management', 'organization', 'computer',
              'SiteOps Software', 'AutoCAD Civil','detention/retention', 'water', 'earthwork analysis', 
              'accounting', 'time', 'time entry', 'business development', 'supervisor',
              'AutoCAD', 'Civil','service restaurant environment','English','SQL', 'visualization',
              'Information Technology','Information Technology','Information Security','IQVIA',
              'sign language interpreter','team building','MS Access', 'MS Word .. PeopleSoft',
              'PC', 'mortgage software','MS Office', 'property management software', 'Yardi',
              'customer service/satisfaction philosophy','issue identification', 'escalation process',
              'Mathematics', 'Life', 'Experience', 'Microsoft','Talent Acquisition','healthcare industry ..',
              'AEM','Prior Sales/Account Management/Performance Management','MS PowerPoint',
               'Sales/Account Management/Performance Management','relationship management', 'account management',
              'customer service orientation'
             ]

def extract_skills(content):
    skills = []
    for skill in word_tokenize(content):
        if skill in all_skills:
            skills.append(skill) if skill not in skills else skills
    return skills


# In[37]:


df_sections = df_sections[df_sections['content'].notna()] # Remove nan rows in content column


# In[38]:


df_sections['content_summerized'] = df_sections.apply(lambda x: extract_intent_skills_based_on_category(x),axis=1)


# In[39]:


df_sections.head()

df_sections.to_csv('section_wise_extracted_and_summerized.csv', index=False)
# # **Questions Searching**

# In[40]:


import selenium
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By


# In[41]:


from googlesearch import search
def search_questions_for(driver,search_keyword, job_title, max_questions = 10):
    search_text = 'top '+search_keyword+' interview questions'
    links = search(search_text, num_results=10)
    raw_questions = []

    for link in links:
        if link.startswith('http'):
            if(len(raw_questions) == max_questions):
                return raw_questions
            try:
                extract_raw_questions(driver, link, raw_questions, max_questions)
            except:
                print('skipping -> '+link)
    return raw_questions


# In[42]:


from string import digits
symbols = "!\"#$%&()*+-./:;<=>?@[\]^_`{|}~\n"    
def pre_process_question(question):
    question = question[0:question.index('?')+1] # extract only question portion
    question = ' '.join(question.split()); # remove multiple spaces
    question = question.lstrip(digits) # remove trailing digits
    question = question.lstrip(symbols) # remove trailing special chars 
    question = question.strip()
    return question

def extract_raw_questions(driver, web_page_link, raw_questions, max_questions):
    try:
        driver.get(web_page_link)
        text_elements_containing_question_mark = driver.find_elements(By.XPATH, "//*[contains(text(), '?')]")
        for question_ele in text_elements_containing_question_mark:
            question = question_ele.text
            if(len(question) != 0):
                if(len(raw_questions) == max_questions):
                    return
                question = pre_process_question(question)
                raw_questions.append(question)
    except Exception as e:
        print('exception occured', e)


# In[52]:


# wd.quit()
opts=webdriver.ChromeOptions()
opts.headless=True
wd = webdriver.Chrome(ChromeDriverManager().install(), options=opts)


# In[44]:


def search_for(df_sections):
    topic_questions = []
    for index, row in df_sections.iterrows():
        t = row['content_summerized']
        job_title = 'Software Developer'
        if type(t) is list:
            for topic in t:
                questions = search_questions_for(wd, topic, job_title, 10)
                for question in questions:
                    topic_questions.append({'topic' : t, 'questions' :question })
        else:
            questions = search_questions_for(wd, t, job_title, 10)
            for question in questions:
                topic_questions.append({'topic' : t, 'questions' :question })
    print(topic_questions)
    return topic_questions


# In[45]:


# df_with_questions_summerized_content.to_csv('section_wise_extracted_and_summerized_with_few_questions.csv', index=False)

# df_with_questions_summerized_content.head(50)
# # **Answering**

# In[46]:



# import Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import re
import csv

"""**Answering**"""

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}

import datetime

class CustomGoogleSearchEngine():
    def __init__(self, question, path_to_document_trunk = "collecteddatafromgoogle.csv"):
        self.document_trunk = path_to_document_trunk
        self.question = question

    def buildDocumentTrunk(self):
        paragraphs = []
        links = self.customsearch(numberOfAnswer = 10)
        with open(self.document_trunk, 'w', encoding="utf8", newline='') as data_source:
            writer = csv.writer(data_source)
            writer.writerow(["question","date", "title", "link", "paragraphs"])
            for link in links:
                try:
                    title, paragraphs = self.paragraphGenerator(link)
                    question=self.question
                    writer.writerow([question,datetime.datetime.now(), title, link, paragraphs])
                except:
                    print('skipping : ', link)
        print("Document trunk was built")
        return self.document_trunk


    def customsearch(self, numberOfAnswer = 10):
        links = []
        for i in search(self.question):
            links.append(i)
        return links


    def chunks(self, l, n):
        for i in range(0, len(l), n):
            yield l[i:i + n]

    def paragraphGenerator(self, url):
        
        req = Request(url, headers=headers) 
        html = urlopen(req, timeout=2).read()
        soup = BeautifulSoup(html, 'html.parser')
        title = soup.find('title')
        for invisible_elem in soup.find_all(['script', 'style', 'head', 'title', 'meta', '[document]']):
            invisible_elem.extract()
        paragraphs = [p.get_text() for p in soup.find_all("p")]
        for para in soup.find_all('p'):
            para.extract()
        for href in soup.find_all(['a','strong']): 
            href.unwrap()
        text = soup.get_text(separator='\n\n')
        text = re.sub('\n +\n','\n\n',text)
        paragraphs += text.split('\n\n')
        paragraphs = [re.sub(' +',' ',p.strip()) for p in paragraphs]
        paragraphs = [p for p in paragraphs if len(p.split()) > 10]

        for i in range(0,len(paragraphs)):
            sents = []
            text_chunks = list(self.chunks(paragraphs[i],1000))
            for chunk in text_chunks:
                sents += sent_tokenize(chunk)
            sents = [s for s in sents if len(s) > 2]
            sents = ' '.join(sents)
            regex_list = re.findall(r"\[(.*?)\]", sents)
            for regex in regex_list:
                sents = sents.replace("["+regex+"]", " ")
            sents = sents.replace("  ", "")
            paragraphs[i] = sents

        if(len(paragraphs) > 10):
            return title, paragraphs[:10]
        return title, paragraphs

# Description : Takes question as an input and process it to find out question and answer type, also prepare question vector and prepare search query for
#   Information Retrieval process
# Arguments : 
#       Input :
#           question(str) : String of question
#           useStemmer(boolean) : Indicate to use stemmer for question tokens
#           useSynonyms(boolean) : Indicate to use thesaraus for query expansion
#           removeStopwords(boolean) : Indicate to remove stop words from search
#                                      query
#       Output :
#           Instance of QuestionProcessor with useful following structure
#               qVector(dict) : Key Value pair of word and its frequency
#                               to be used for Information Retrieval and 
#                               similarity calculation
#               question(str) : Raw question
#               qType(str) : Type of question
#               aType(str) : Expected answer type
#                       ["PERSON","LOCATION","DATE","DEFINITION","YESNO"]
#               

from nltk import pos_tag,word_tokenize,ne_chunk
from nltk.stem.porter import PorterStemmer
import nltk
# nltk.download('stopwords')
# nltk.download('averaged_perceptron_tagger')
from nltk.corpus import wordnet,stopwords

class QuestionProcessor:
    def __init__(self, question, useStemmer = False, useSynonyms = False, removeStopwords = False):
        self.question = question
        self.useStemmer = useStemmer
        self.useSynonyms = useSynonyms
        self.removeStopwords = removeStopwords
        self.stopWords = stopwords.words("english")
        self.stem = lambda k : k.lower()
        if self.useStemmer:
            ps = PorterStemmer()
            self.stem = ps.stem
        self.qType = self.determineQuestionType(question)
        self.searchQuery = self.buildSearchQuery(question)
        self.qVector = self.getQueryVector(self.searchQuery)
        self.aType = self.determineAnswerType(question)
    
    # To determine type of question by analyzing POS tag of question from tagset
    #
    # Input:
    #           question(str) : Question string
    # Output:
    #           qType(str) : Type of question mentioned below
    #                   [ WP ->  who
    #                     WDT -> what, why, how
    #                     WP$ -> whose
    #                     WRB -> where ]
    def determineQuestionType(self, question):
        questionTaggers = ['WP','WDT','WP$','WRB']
        qPOS = pos_tag(word_tokenize(question))
        qTags = []
        for token in qPOS:
            if token[1] in questionTaggers:
                qTags.append(token[1])
        qType = ''
        if(len(qTags)>1):
            qType = 'complex'
        elif(len(qTags) == 1):
            qType = qTags[0]
        else:
            qType = "None"
        return qType
    
    # To determine type of expected answer depending of question type
    #
    # Input:
    #           question(str) : Question string
    # Output:
    #           aType(str) : Type of answer among following
    #               [PERSON, LOCATION, DATE, ORGANIZATION, QUANTITY, DEFINITION
    #                   FULL]
    def determineAnswerType(self, question):
        questionTaggers = ['WP','WDT','WP$','WRB']
        qPOS = pos_tag(word_tokenize(question))
        qTag = None

        for token in qPOS:
            if token[1] in questionTaggers:
                qTag = token[0].lower()
                break
        
        if(qTag == None):
            if len(qPOS) > 1:
                if qPOS[1][1].lower() in ['is','are','can','should']:
                    qTag = "YESNO"
        #who/where/what/why/when/is/are/can/should
        if qTag == "who":
            return "PERSON"
        elif qTag == "where":
            return "LOCATION"
        elif qTag == "when":
            return "DATE"
        elif qTag == "what":
            # Defination type question
            # If question of type whd modal noun? its a definition question
            qTok = self.getContinuousChunk(question)
            #print(qTok)
            if(len(qTok) == 4):
                if qTok[1][1] in ['is','are','was','were'] and qTok[2][0] in ["NN","NNS","NNP","NNPS"]:
                    self.question = " ".join([qTok[0][1],qTok[2][1],qTok[1][1]])
                    #print("Type of question","Definition",self.question)
                    return "DEFINITION"

            # ELSE USE FIRST HEAD WORD
            for token in qPOS:
                if token[0].lower() in ["city","place","country"]:
                    return "LOCATION"
                elif token[0].lower() in ["company","industry","organization"]:
                    return "ORGANIZATION"
                elif token[1] in ["NN","NNS"]:
                    return "FULL"
                elif token[1] in ["NNP","NNPS"]:
                    return "FULL"
            return "FULL"
        elif qTag == "how":
            if len(qPOS)>1:
                t2 = qPOS[2]
                if t2[0].lower() in ["few","great","little","many","much"]:
                    return "QUANTITY"
                elif t2[0].lower() in ["tall","wide","big","far"]:
                    return "LINEAR_MEASURE"
            return "FULL"
        else:
            return "FULL"
    
    # To build search query by dropping question word
    #
    # Input:
    #           question(str) : Question string
    # Output:
    #           searchQuery(list) : List of tokens
    def buildSearchQuery(self, question):
        qPOS = pos_tag(word_tokenize(question))
        searchQuery = []
        questionTaggers = ['WP','WDT','WP$','WRB']
        for tag in qPOS:
            if tag[1] in questionTaggers:
                continue
            else:
                searchQuery.append(tag[0])
                if(self.useSynonyms):
                    syn = self.getSynonyms(tag[0])
                    if(len(syn) > 0):
                        searchQuery.extend(syn)
        return searchQuery
    
    # To build query vector
    #
    # Input:
    #       searchQuery(list) : List of tokens from buildSearchQuery method
    # Output:
    #       qVector(dict) : Dictionary of words and their frequency
    def getQueryVector(self, searchQuery):
        vector = {}
        for token in searchQuery:
            if self.removeStopwords:
                if token in self.stopWords:
                    continue
            token = self.stem(token)
            if token in vector.keys():
                vector[token] += 1
            else:
                vector[token] = 1
        return vector
    
    # To get continuous chunk of similar POS tags.
    # E.g.  If two NN tags are consequetive, this method will merge and return single NN with combined value.
    #       It is helpful in detecting name of single person like Sachin Tendulkar, Steve Jobs
    # Input:
    #       question(str) : question string
    # Output:
    #       
    def getContinuousChunk(self,question):
        chunks = []
        answerToken = word_tokenize(question)
        nc = pos_tag(answerToken)

        prevPos = nc[0][1]
        entity = {"pos":prevPos,"chunk":[]}
        for c_node in nc:
            (token,pos) = c_node
            if pos == prevPos:
                prevPos = pos       
                entity["chunk"].append(token)
            elif prevPos in ["DT","JJ"]:
                prevPos = pos
                entity["pos"] = pos
                entity["chunk"].append(token)
            else:
                if not len(entity["chunk"]) == 0:
                    chunks.append((entity["pos"]," ".join(entity["chunk"])))
                    entity = {"pos":pos,"chunk":[token]}
                    prevPos = pos
        if not len(entity["chunk"]) == 0:
            chunks.append((entity["pos"]," ".join(entity["chunk"])))
        return chunks
    
    # To get synonyms of word in order to improve query by using query expanision technique
    # Input:
    #       word(str) : Word token
    # Output:
    #       synonyms(list) : List of synonyms of given word
    def getSynonyms(self, word):
        synonyms = []
        for syn in wordnet.synsets(word):
            for l in syn.lemmas():
                w = l.name().lower()
                synonyms.extend(w.split("_"))
        return list(set(synonyms))
    
    # String representation of this class
    def __repr__(self):
        msg = "Q: " + self.question + "\n"
        msg += "QType: " + self.qType + "\n"
        msg += "QVector: " + str(self.qVector) + "\n"
        return msg

import nltk
nltk.download('punkt')
from nltk import sent_tokenize
import re
from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
import requests
from googlesearch import search

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}

class ParagraphGenerator:
    def __init__(self, question):
        self.paragraphs = self.paragraphsGenerator(question)

    def ggSearch(self, question):
        links = []
        prevLink = ""
        for j in search(question, num_results=10):
            curLink = re.findall(r"\//(.*?)\/", j)[0]
            if (curLink != prevLink):
                links.append(j)
                prevLink = curLink
        return links

    def chunks(self, l, n):
        for i in range(0, len(l), n):
            yield l[i:i + n]

    def paragraphSearch(self, url):
        req = Request(url, headers=headers) 
        html = urlopen(req).read()
        soup = BeautifulSoup(html, 'lxml')
        for invisible_elem in soup.find_all(['script', 'style']):
            invisible_elem.extract()

        paragraphs = [p.get_text() for p in soup.find_all("p")]
        for para in soup.find_all('p'):
            para.extract()

        for href in soup.find_all(['a','strong']): 
            href.unwrap()
        text = soup.get_text(separator='\n\n')
        text = re.sub('\n +\n','\n\n',text)

        paragraphs += text.split('\n\n')
        paragraphs = [re.sub(' +',' ',p.strip()) for p in paragraphs[:5]]
        paragraphs = [p for p in paragraphs if len(p.split()) > 50]

        # for i in range(0,len(paragraphs)):
        #     sents = []
        #     text_chunks = list(self.chunks(paragraphs[i],10000))
        #     for chunk in text_chunks:
        #         sents += sent_tokenize(chunk)
        #     sents = [s for s in sents if len(s) > 2]
        #     sents = ' '.join(sents)
        #     paragraphs[i] = sents

        paragraph = '\n\n'.join(paragraphs)
        regex_list = re.findall(r"\[(.*?)\]", paragraph)
        for regex in regex_list:
            paragraph = paragraph.replace("["+regex+"]", " ")
        regex_list = re.findall(r"\((.*?)\)", paragraph)
        for regex in regex_list:
            paragraph = paragraph.replace("("+regex+")", " ")
        #paragraph = paragraph.replace("  ", "")

        return paragraph

    def paragraphsGenerator(self, question):
        paras = []
        links = self.ggSearch(question)
        for link in links:
            para = self.paragraphSearch(link)
            if(para != ""):
                paras.append(para)
        return paras[0]

# Description : Script preprocesses article and paragraph to computer TFIDF and helps in answer processing 
# Arguments : 
#       Input :
#           paragraphs(list)        : List of paragraphs
#           useStemmer(boolean)     : Indicate to use stemmer for word tokens
#           removeStopWord(boolean) : Indicate to remove stop words from 
#                                     paragraph in order to keep relevant words
#       Output :
#           Instance of DocumentRetrievalModel with following structure
#               query(function) : Take instance of processedQuestion and return
#                                 answer based on IR and Answer Processing
#                                 techniques

# Importing Library
from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem.porter import PorterStemmer
from nltk.tree import Tree
from nltk import pos_tag,ne_chunk
import json
import math
import re

class DocumentRetrievalModel:
    def __init__(self,paragraphs,removeStopWord = False,useStemmer = False):
        self.idf = {}               # dict to store IDF for words in paragraph
        self.paragraphInfo = {}     # structure to store paragraphVector
        self.paragraphs = paragraphs
        self.totalParas = len(paragraphs)
        self.stopwords = stopwords.words('english')
        self.removeStopWord = removeStopWord
        self.useStemmer = useStemmer
        self.vData = None
        self.stem = lambda k:k.lower()
        if(useStemmer):
            ps = PorterStemmer()
            self.stem = ps.stem
            
        # Initialize
        self.computeTFIDF()
        
    # Return term frequency for Paragraph
    # Input:
    #       paragraph(str): Paragraph as a whole in string format
    # Output:
    #       wordFrequence(dict) : Dictionary of word and term frequency
    def getTermFrequencyCount(self,paragraph):
        sentences = sent_tokenize(paragraph)
        wordFrequency = {}
        for sent in sentences:
            for word in word_tokenize(sent):
                if self.removeStopWord == True:
                    if word.lower() in self.stopwords:
                        #Ignore stopwords
                        continue
                    if not re.match(r"[a-zA-Z0-9\-\_\\/\.\']+",word):
                        continue
                #Use of Stemmer
                if self.useStemmer:
                    word = self.stem(word)
                    
                if word in wordFrequency.keys():
                    wordFrequency[word] += 1
                else:
                    wordFrequency[word] = 1
        return wordFrequency
    
    # Computes term-frequency inverse document frequency for every token of each paragraph
    # Output:
    #       paragraphInfo(dict): Dictionary for every paragraph with following keys
    #                   vector : dictionary of TFIDF for every word
    def computeTFIDF(self):
        # Compute Term Frequency
        self.paragraphInfo = {}
        for index in range(0,len(self.paragraphs)):
            wordFrequency = self.getTermFrequencyCount(self.paragraphs[index])
            self.paragraphInfo[index] = {}
            self.paragraphInfo[index]['wF'] = wordFrequency
        
        wordParagraphFrequency = {}
        for index in range(0,len(self.paragraphInfo)):
            for word in self.paragraphInfo[index]['wF'].keys():
                if word in wordParagraphFrequency.keys():
                    wordParagraphFrequency[word] += 1
                else:
                    wordParagraphFrequency[word] = 1
        
        self.idf = {}
        for word in wordParagraphFrequency:
            # Adding Laplace smoothing by adding 1 to total number of documents
            self.idf[word] = math.log((self.totalParas+1)/wordParagraphFrequency[word])
        
        #Compute Paragraph Vector
        for index in range(0,len(self.paragraphInfo)):
            self.paragraphInfo[index]['vector'] = {}
            for word in self.paragraphInfo[index]['wF'].keys():
                self.paragraphInfo[index]['vector'][word] = self.paragraphInfo[index]['wF'][word] * self.idf[word]
    

    # To find answer to the question by first finding relevant paragraph, then by finding relevant sentence and then by procssing sentence to get answer
    # based on expected answer type
    # Input:
    #           pQ(ProcessedQuestion) : Instance of ProcessedQuestion
    # Output:
    #           answer(str) : Response of QA System
    def query(self,pQ):
        
        # Get relevant Paragraph
        relevantParagraph = self.getSimilarParagraph(pQ.qVector)

        # Get All sentences
        targetParagraph = ""
        sentences = []
        for tup in relevantParagraph:
            if tup != None:
                targetParagraph = self.paragraphs[tup[0]]
                sentences.extend(sent_tokenize(targetParagraph))
        # # Get Relevant Sentences
        # if len(sentences) == 0:
        #     return "Oops! Unable to find answer"

        # # Get most relevant sentence using unigram similarity
        # relevantSentences = self.getMostRelevantSentences(sentences,pQ,1)

        return targetParagraph
        
    # Get top 3 relevant paragraph based on cosine similarity between question vector and paragraph vector
    # Input :
    #       queryVector(dict) : Dictionary of words in question with their 
    #                           frequency
    # Output:
    #       pRanking(list) : List of tuple with top 3 paragraph with its
    #                        similarity coefficient
    def getSimilarParagraph(self,queryVector):    
        queryVectorDistance = 0
        for word in queryVector.keys():
            if word in self.idf.keys():
                queryVectorDistance += math.pow(queryVector[word]*self.idf[word],2)
        queryVectorDistance = math.pow(queryVectorDistance,0.5)
        if queryVectorDistance == 0:
            return [None]
        pRanking = []
        for index in range(0,len(self.paragraphInfo)):
            sim = self.computeSimilarity(self.paragraphInfo[index], queryVector, queryVectorDistance)
            pRanking.append((index,sim))
        
        return sorted(pRanking,key=lambda tup: (tup[1],tup[0]), reverse=True)[:3]
    
    # Compute cosine similarity betweent queryVector and paragraphVector
    # Input:
    #       pInfo(dict)         : Dictionary containing wordFrequency and 
    #                             paragraph Vector
    #       queryVector(dict)   : Query vector for question
    #       queryDistance(float): Distance of queryVector from origin
    # Output:
    #       sim(float)          : Cosine similarity coefficient
    def computeSimilarity(self, pInfo, queryVector, queryDistance):
        # Computing pVectorDistance
        pVectorDistance = 0
        for word in pInfo['wF'].keys():
            pVectorDistance += math.pow(pInfo['wF'][word]*self.idf[word],2)
        pVectorDistance = math.pow(pVectorDistance,0.5)
        if(pVectorDistance == 0):
            return 0

        # Computing dot product
        dotProduct = 0
        for word in queryVector.keys():
            if word in pInfo['wF']:
                q = queryVector[word]
                w = pInfo['wF'][word]
                idf = self.idf[word]
                dotProduct += q*w*idf*idf
        
        sim = dotProduct / (pVectorDistance * queryDistance)
        return sim
    
    # Get most relevant sentences using unigram similarity between question
    # sentence and sentence in paragraph containing potential answer
    # Input:
    #       sentences(list)      : List of sentences in order of occurance as in
    #                              paragraph
    #       pQ(ProcessedQuestion): Instance of processedQuestion
    #       nGram(int)           : Value of nGram (default 3)
    # Output:
    #       relevantSentences(list) : List of tuple with sentence and their
    #                                 similarity coefficient
    def getMostRelevantSentences(self, sentences, pQ, nGram=3):
        relevantSentences = []
        for sent in sentences:
            sim = 0
            if(len(word_tokenize(pQ.question))>nGram+1):
                sim = self.sim_ngram_sentence(pQ.question,sent,nGram)
            else:
                sim = self.sim_sentence(pQ.qVector, sent)
            relevantSentences.append((sent,sim))
        
        return sorted(relevantSentences,key=lambda tup:(tup[1],tup[0]),reverse=True)
    
    # Compute ngram similarity between a sentence and question
    # Input:
    #       question(str)   : Question string
    #       sentence(str)   : Sentence string
    #       nGram(int)      : Value of n in nGram
    # Output:
    #       sim(float)      : Ngram Similarity Coefficient
    def sim_ngram_sentence(self, question, sentence,nGram):
        #considering stop words as well
        ps = PorterStemmer()
        getToken = lambda question:[ ps.stem(w.lower()) for w in word_tokenize(question) ]
        getNGram = lambda tokens,n:[ " ".join([tokens[index+i] for i in range(0,n)]) for index in range(0,len(tokens)-n+1)]
        qToken = getToken(question)
        sToken = getToken(sentence)

        if(len(qToken) > nGram):
            q3gram = set(getNGram(qToken,nGram))
            s3gram = set(getNGram(sToken,nGram))
            if(len(s3gram) < nGram):
                return 0
            sim = len(q3gram.intersection(s3gram)) / len(q3gram.union(s3gram))
            return sim
        else:
            return 0
    
    # Compute similarity between sentence and queryVector based on number of 
    # common words in both sentence. It doesn't consider occurance of words
    # Input:
    #       queryVector(dict)   : Dictionary of words in question
    #       sentence(str)       : Sentence string
    # Ouput:
    #       sim(float)          : Similarity Coefficient    
    def sim_sentence(self, queryVector, sentence):
        sentToken = word_tokenize(sentence)
        ps = PorterStemmer()
        for index in range(0,len(sentToken)):
            sentToken[index] = ps.stem(sentToken[index])
        sim = 0
        for word in queryVector.keys():
            w = ps.stem(word)
            if w in sentToken:
                sim += 1
        return sim/(len(sentToken)*len(queryVector.keys()))
    
    # Get Named Entity from the sentence in form of PERSON, GPE, & ORGANIZATION
    # Input:
    #       answers(list)       : List of potential sentence containing answer
    # Output:
    #       chunks(list)        : List of tuple with entity and name in ranked 
    #                             order
    def getNamedEntity(self,answers):
        chunks = []
        for answer in answers:
            answerToken = word_tokenize(answer)
            nc = ne_chunk(pos_tag(answerToken))
            entity = {"label":None,"chunk":[]}
            for c_node in nc:
                if(type(c_node) == Tree):
                    if(entity["label"] == None):
                        entity["label"] = c_node.label()
                    entity["chunk"].extend([ token for (token,pos) in c_node.leaves()])
                else:
                    (token,pos) = c_node
                    if pos == "NNP":
                        entity["chunk"].append(token)
                    else:
                        if not len(entity["chunk"]) == 0:
                            chunks.append((entity["label"]," ".join(entity["chunk"])))
                            entity = {"label":None,"chunk":[]}
            if not len(entity["chunk"]) == 0:
                chunks.append((entity["label"]," ".join(entity["chunk"])))
        return chunks
    
    # To get continuous chunk of similar POS tags.
    # E.g.  If two NN tags are consequetive, this method will merge and return
    #       single NN with combined value.
    #       It is helpful in detecting name of single person like John Cena, 
    #       Steve Jobs
    # Input:
    #       answers(list) : list of potential sentence string
    # Output:
    #       chunks(list)  : list of tuple with entity and name in ranked order
    def getContinuousChunk(self,answers):
        chunks = []
        for answer in answers:
            answerToken = word_tokenize(answer)
            if(len(answerToken)==0):
                continue
            nc = pos_tag(answerToken)
            
            prevPos = nc[0][1]
            entity = {"pos":prevPos,"chunk":[]}
            for c_node in nc:
                (token,pos) = c_node
                if pos == prevPos:
                    prevPos = pos       
                    entity["chunk"].append(token)
                elif prevPos in ["DT","JJ"]:
                    prevPos = pos
                    entity["pos"] = pos
                    entity["chunk"].append(token)
                else:
                    if not len(entity["chunk"]) == 0:
                        chunks.append((entity["pos"]," ".join(entity["chunk"])))
                        entity = {"pos":pos,"chunk":[token]}
                        prevPos = pos
            if not len(entity["chunk"]) == 0:
                chunks.append((entity["pos"]," ".join(entity["chunk"])))
        return chunks
    
    def getqRev(self, pq):
        if self.vData == None:
            # For testing purpose
            self.vData = json.loads(open("validatedata.py","r").readline())
        revMatrix = []
        for t in self.vData:
            sent = t["q"]
            revMatrix.append((t["a"],self.sim_sentence(pq.qVector,sent)))
        return sorted(revMatrix,key=lambda tup:(tup[1],tup[0]),reverse=True)[0][0]

# Commented out IPython magic to ensure Python compatibility.
import numpy as np
import scipy.sparse as sp
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_is_fitted, check_array, FLOAT_DTYPES
from sklearn.feature_extraction.text import _document_frequency
from sklearn.preprocessing import normalize


class BM25Transformer(BaseEstimator, TransformerMixin):
    """
    Parameters
    ----------
     norm : 'l1', 'l2' or None, optional (default=None)
        Each output row will have unit norm, either:
        * 'l2': Sum of squares of vector elements is 1. The cosine similarity between two vectors is their dot product when l2 norm has been applied.
        * 'l1': Sum of absolute values of vector elements is 1.
    use_idf : boolean, optional (default=True)
        Enable inverse-document-frequency reweighting
    k1 : float, optional (default=2.0)
        term k1 in the BM25 formula
    b : float, optional (default=0.75)
        term b in the BM25 formula
    floor : float or None, optional (default=None)
        floor value for idf terms
    
    """

    def __init__(self, norm=None, use_idf=True, k1=2.0, b=0.75, floor=None):
        self.norm = norm
        self.use_idf = use_idf
        self.k1 = k1
        self.b = b
        self.floor = floor

    def fit(self, X):
        """
        Parameters
        ----------
        X : sparse matrix, [n_samples, n_features]
            document-term matrix
        """
        X = check_array(X, accept_sparse=("csr", "csc"))
        if not sp.issparse(X):
            X = sp.csc_matrix(X)
        if self.use_idf:
            n_samples, n_features = X.shape
            df = _document_frequency(X)
            idf = np.log((n_samples - df + 0.5) / (df + 0.5))
            if self.floor is not None:
                idf = idf * (idf > self.floor) + self.floor * (idf < self.floor)
            self._idf_diag = sp.spdiags(idf, diags=0, m=n_features, n=n_features)

        # Create BM25 features

        # Document length (number of terms) in each row
        # Shape is (n_samples, 1)
        dl = X.sum(axis=1)
        # Number of non-zero elements in each row
        # Shape is (n_samples, )
        sz = X.indptr[1:] - X.indptr[0:-1]
        # In each row, repeat `dl` for `sz` times
        # Shape is (sum(sz), )
        rep = np.repeat(np.asarray(dl), sz)
        # Average document length
        # Scalar value
        avgdl = np.average(dl)
        # Compute BM25 score only for non-zero elements
        data = (
            X.data
            * (self.k1 + 1)
            / (X.data + self.k1 * (1 - self.b + self.b * rep / avgdl))
        )
        X = sp.csr_matrix((data, X.indices, X.indptr), shape=X.shape)

        if self.norm:
            X = normalize(X, norm=self.norm, copy=False)

        self._doc_matrix = X
        return self

    def transform(self, X=None, copy=True, is_query=False):
        """
        Parameters
        ----------
        X : sparse matrix, [n_samples, n_features] document-term query matrix
        copy : boolean, optional (default=True)
        query: boolean (default=False)
            whether to transform a query or the documents database
        Returns
        -------
        vectors : sparse matrix, [n_samples, n_features]
        """
        if is_query:
            X = check_array(X, accept_sparse="csr", dtype=FLOAT_DTYPES, copy=copy)
            if not sp.issparse(X):
                X = sp.csr_matrix(X, dtype=np.float64)

            n_samples, n_features = X.shape

            expected_n_features = self._doc_matrix.shape[1]
            if n_features != expected_n_features:
                raise ValueError(
                    "Input has n_features=%d while the model"
                    " has been trained with n_features=%d"
#                     % (n_features, expected_n_features)
                )

            if self.use_idf:
                check_is_fitted(self, "_idf_diag")
                X = sp.csr_matrix(X.toarray() * self._idf_diag.diagonal())

            return X

        else:
            return self._doc_matrix

    @property
    def idf_(self):
        # if _idf_diag is not set, this will raise an attribute error,
        # which means hasattr(self, "idf_") is False
        return np.ravel(self._idf_diag.sum(axis=0))

    @idf_.setter
    def idf_(self, value):
        value = np.asarray(value, dtype=np.float64)
        n_features = value.shape[0]
        self._idf_diag = sp.spdiags(
            value, diags=0, m=n_features, n=n_features, format="csr"
        )

import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
#from cdqadocumentretriever.text_transformers import BM25Transformer


class BM25Vectorizer(CountVectorizer):
    """Convert a collection of raw documents to a matrix of BM25 features and computes scores of the documents based on a query
    Vectorizer inspired on the sklearn.feature_extraction.text.TfidfVectorizer class
    Parameters
    ----------
    input : string {'filename', 'file', 'content'}
        If 'filename', the sequence passed as an argument to fit is
        expected to be a list of filenames that need reading to fetch
        the raw content to analyze.
        If 'file', the sequence items must have a 'read' method (file-like
        object) that is called to fetch the bytes in memory.
        Otherwise the input is expected to be the sequence strings or
        bytes items are expected to be analyzed directly.
    encoding : string, 'utf-8' by default.
        If bytes or files are given to analyze, this encoding is used to
        decode.
    decode_error : {'strict', 'ignore', 'replace'} (default='strict')
        Instruction on what to do if a byte sequence is given to analyze that
        contains characters not of the given `encoding`. By default, it is
        'strict', meaning that a UnicodeDecodeError will be raised. Other
        values are 'ignore' and 'replace'.
    strip_accents : {'ascii', 'unicode', None} (default=None)
        Remove accents and perform other character normalization
        during the preprocessing step.
        'ascii' is a fast method that only works on characters that have
        an direct ASCII mapping.
        'unicode' is a slightly slower method that works on any characters.
        None (default) does nothing.
        Both 'ascii' and 'unicode' use NFKD normalization from
        :func:`unicodedata.normalize`.
    lowercase : boolean (default=True)
        Convert all characters to lowercase before tokenizing.
    preprocessor : callable or None (default=None)
        Override the preprocessing (string transformation) stage while
        preserving the tokenizing and n-grams generation steps.
    tokenizer : callable or None (default=None)
        Override the string tokenization step while preserving the
        preprocessing and n-grams generation steps.
        Only applies if ``analyzer == 'word'``.
    analyzer : string, {'word', 'char', 'char_wb'} or callable
        Whether the feature should be made of word or character n-grams.
        Option 'char_wb' creates character n-grams only from text inside
        word boundaries; n-grams at the edges of words are padded with space.
        If a callable is passed it is used to extract the sequence of features
        out of the raw, unprocessed input.
        .. versionchanged:: 0.21
        Since v0.21, if ``input`` is ``filename`` or ``file``, the data is
        first read from the file and then passed to the given callable
        analyzer.
    stop_words : string {'english'}, list, or None (default=None)
        If a string, it is passed to _check_stop_list and the appropriate stop
        list is returned. 'english' is currently the only supported string
        value.
        There are several known issues with 'english' and you should
        consider an alternative (see :ref:`stop_words`).
        If a list, that list is assumed to contain stop words, all of which
        will be removed from the resulting tokens.
        Only applies if ``analyzer == 'word'``.
        If None, no stop words will be used. max_df can be set to a value
        in the range [0.7, 1.0) to automatically detect and filter stop
        words based on intra corpus document frequency of terms.
    token_pattern : string
        Regular expression denoting what constitutes a "token", only used
        if ``analyzer == 'word'``. The default regexp selects tokens of 2
        or more alphanumeric characters (punctuation is completely ignored
        and always treated as a token separator).
    ngram_range : tuple (min_n, max_n) (default=(1, 1))
        The lower and upper boundary of the range of n-values for different
        n-grams to be extracted. All values of n such that min_n <= n <= max_n
        will be used.
    max_df : float in range [0.0, 1.0] or int (default=1.0)
        When building the vocabulary ignore terms that have a document
        frequency strictly higher than the given threshold (corpus-specific
        stop words).
        If float, the parameter represents a proportion of documents, integer
        absolute counts.
        This parameter is ignored if vocabulary is not None.
    min_df : float in range [0.0, 1.0] or int (default=1)
        When building the vocabulary ignore terms that have a document
        frequency strictly lower than the given threshold. This value is also
        called cut-off in the literature.
        If float, the parameter represents a proportion of documents, integer
        absolute counts.
        This parameter is ignored if vocabulary is not None.
    max_features : int or None (default=None)
        If not None, build a vocabulary that only consider the top
        max_features ordered by term frequency across the corpus.
        This parameter is ignored if vocabulary is not None.
    vocabulary : Mapping or iterable, optional (default=None)
        Either a Mapping (e.g., a dict) where keys are terms and values are
        indices in the feature matrix, or an iterable over terms. If not
        given, a vocabulary is determined from the input documents.
    binary : boolean (default=False)
        If True, all non-zero term counts are set to 1. This does not mean
        outputs will have only 0/1 values, only that the tf term in tf-idf
        is binary. (Set idf and normalization to False to get 0/1 outputs.)
    dtype : type, optional (default=float64)
        Type of the matrix returned by fit_transform() or transform().
    norm : 'l1', 'l2' or None, optional (default='l2')
        Each output row will have unit norm, either:
        * 'l2': Sum of squares of vector elements is 1. The cosine
        similarity between two vectors is their dot product when l2 norm has
        been applied.
        * 'l1': Sum of absolute values of vector elements is 1.
        See :func:`preprocessing.normalize`
    use_idf : boolean (default=True)
        Enable inverse-document-frequency reweighting.
    k1 : float, optional (default=2.0)
        term k1 in the BM25 formula
    b : float, optional (default=0.75)
        term b in the BM25 formula
    floor : float or None, optional (default=None)
        floor value for idf terms
    Attributes
    ----------
    vocabulary_ : dict
        A mapping of terms to feature indices.
    idf_ : array, shape (n_features)
        The inverse document frequency (IDF) vector; only defined
        if ``use_idf`` is True.
    stop_words_ : set
        Terms that were ignored because they either:
          - occurred in too many documents (`max_df`)
          - occurred in too few documents (`min_df`)
          - were cut off by feature selection (`max_features`).
        This is only available if no vocabulary was given.
    """

    def __init__(
        self,
        input="content",
        encoding="utf-8",
        decode_error="strict",
        strip_accents=None,
        lowercase=True,
        preprocessor=None,
        tokenizer=None,
        analyzer="word",
        stop_words=None,
        token_pattern=r"(?u)\b\w\w+\b",
        ngram_range=(1, 2),
        max_df=1.0,
        min_df=1,
        max_features=None,
        vocabulary=None,
        binary=False,
        dtype=np.float64,
        norm=None,
        use_idf=True,
        k1=2.0,
        b=0.75,
        floor=None,
    ):

        super().__init__(
            input=input,
            encoding=encoding,
            decode_error=decode_error,
            strip_accents=strip_accents,
            lowercase=lowercase,
            preprocessor=preprocessor,
            tokenizer=tokenizer,
            analyzer=analyzer,
            stop_words=stop_words,
            token_pattern=token_pattern,
            ngram_range=ngram_range,
            max_df=max_df,
            min_df=min_df,
            max_features=max_features,
            vocabulary=vocabulary,
            binary=binary,
            dtype=dtype,
        )

        self._bm25 = BM25Transformer(norm, use_idf, k1, b)

    # Broadcast the BM25 parameters to the underlying transformer instance
    # for easy grid search and repr
    @property
    def norm(self):
        return self._bm25.norm

    @norm.setter
    def norm(self, value):
        self._bm25.norm = value

    @property
    def use_idf(self):
        return self._bm25.use_idf

    @use_idf.setter
    def use_idf(self, value):
        self._bm25.use_idf = value

    @property
    def k1(self):
        return self._bm25.k1

    @k1.setter
    def k1(self, value):
        self._bm25.k1 = value

    @property
    def b(self):
        return self._bm25.b

    @b.setter
    def b(self, value):
        self._bm25.b = value

    @property
    def idf_(self):
        return self._bm25.idf_

    @idf_.setter
    def idf_(self, value):
        self._validate_vocabulary()
        if hasattr(self, "vocabulary_"):
            if len(self.vocabulary_) != len(value):
                raise ValueError(
                    "idf length = %d must be equal "
                    "to vocabulary size = %d" % (len(value), len(self.vocabulary))
                )
        self._bm25.idf_ = value

    def fit(self, raw_documents, y=None):
        """
        Learn vocabulary and BM25 stats from training set.
        Parameters
        ----------
        raw_documents : iterable
            an iterable which yields either str, unicode or file objects
        Returns
        -------
        self : BM25Vectorizer
        """
        X = super().fit_transform(raw_documents)
        self._bm25.fit(X)
        return self

    def transform(self, raw_corpus, is_query=False):
        """
        Vectorizes the input, whether it is a query or the list of documents
        Parameters
        ----------
        raw_corpus : iterable
            an iterable which yields either str, unicode or file objects
        Returns
        -------
        vectors : sparse matrix, [n_queries, n_documents]
            scores from BM25 statics for each document with respect to each query
        """
        X = super().transform(raw_corpus) if is_query else None

        return self._bm25.transform(X, copy=False, is_query=is_query)

    def fit_transform(self, raw_documents, y=None):
        """
        Learn vocabulary, idf and BM25 features. Return term-document matrix.
        This is equivalent to fit followed by transform, but more efficiently
        implemented.
        Parameters
        ----------
        raw_documents : iterable
            an iterable which yields either str, unicode or file objects
            
        Returns
        -------
        X : sparse matrix, [n_samples, n_features]
            BM25 document-term matrix.
        """
        X = super().fit_transform(raw_documents)
        self._bm25.fit(X)
        return self._bm25.transform(X, copy=False)

import pandas as pd
import prettytable
import time
from abc import ABC, abstractmethod
from collections import OrderedDict
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.base import BaseEstimator
#from .vectorizers import BM25Vectorizer


class BaseRetriever(BaseEstimator, ABC):
    """
    Abstract base class for all Retriever classes.
    All retrievers should inherit from this class.
    Each retriever class should implement a _fit_vectorizer method and a _compute_scores method
    """

    def __init__(self, vectorizer, top_n=10, verbose=False):
        self.vectorizer = vectorizer
        self.top_n = top_n
        self.verbose = verbose

    def fit(self, df: pd.DataFrame, y=None):
        """
        Fit the retriever to a list of documents or paragraphs
        Parameters
        ----------
        df: pandas.DataFrame object with all documents
        """
        self.metadata = df
        return self._fit_vectorizer(df)

    @abstractmethod
    def _fit_vectorizer(self, df):
        pass

    @abstractmethod
    def _compute_scores(self, query):
        pass

    def predict(self, query: str) -> OrderedDict:
        """
        Compute the top_n closest documents given a query
        Parameters
        ----------
        query: str
        Returns
        -------
        best_idx_scores: OrderedDict
            Dictionnaire with top_n best scores and idices of the documents as keys
        """
        t0 = time.time()
        scores = self._compute_scores(query)
        idx_scores = [(idx, score) for idx, score in enumerate(scores)]
        best_idx_scores = OrderedDict(
            sorted(idx_scores, key=(lambda tup: tup[1]), reverse=True)[: self.top_n]
        )

        # # inspired from https://github.com/facebookresearch/DrQA/blob/50d0e49bb77fe0c6e881efb4b6fe2e61d3f92509/scripts/reader/interactive.py#L63
        # if self.verbose:
        #     rank = 1
        #     table = prettytable.PrettyTable(["rank", "index", "title"])
        #     for i in range(len(best_idx_scores)):
        #         index = best_idx_scores[i]
        #         if self.paragraphs:
        #             article_index = self.paragraphs[int(index)]["index"]
        #             title = self.metadata.iloc[int(article_index)]["title"]
        #         else:
        #             title = self.metadata.iloc[int(index)]["title"]
        #         table.add_row([rank, index, title])
        #         rank += 1
        #     print(table)
        print("Time: {} seconds".format(round(time.time() - t0, 5)))

        return best_idx_scores

class TfidfRetriever(BaseRetriever):
    """
    A scikit-learn estimator for TfidfRetriever. Trains a tf-idf matrix from a corpus
    of documents then finds the most N similar documents of a given input document by
    taking the dot product of the vectorized input document and the trained tf-idf matrix.
    Parameters
    ----------
    lowercase : boolean
        Convert all characters to lowercase before tokenizing. (default is True)
    preprocessor : callable or None
        Override the preprocessing (string transformation) stage while preserving
        the tokenizing and n-grams generation steps. (default is None)
    tokenizer : callable or None
        Override the string tokenization step while preserving the preprocessing
        and n-grams generation steps (default is None)
    stop_words : string {‘english’}, list, or None
        If a string, it is passed to _check_stop_list and the appropriate stop
        list is returned. ‘english’ is currently the only supported string value.
        If a list, that list is assumed to contain stop words, all of which will
        be removed from the resulting tokens.
        If None, no stop words will be used. max_df can be set to a value in the
        range [0.7, 1.0) to automatically detect and filter stop words based on
        intra corpus document frequency of terms.
        (default is None)
    token_pattern : string
        Regular expression denoting what constitutes a “token”. The default regexp
        selects tokens of 2 or more alphanumeric characters (punctuation is completely
        ignored and always treated as a token separator).
    ngram_range : tuple (min_n, max_n)
        The lower and upper boundary of the range of n-values for different n-grams
        to be extracted. All values of n such that min_n <= n <= max_n will be used.
        (default is (1, 1))
    max_df : float in range [0.0, 1.0] or int
        When building the vocabulary ignore terms that have a document frequency strictly
        higher than the given threshold (corpus-specific stop words). If float, the parameter
        represents a proportion of documents, integer absolute counts. This parameter is
        ignored if vocabulary is not None. (default is 1.0)
    min_df : float in range [0.0, 1.0] or int
        When building the vocabulary ignore terms that have a document frequency
        strictly lower than the given threshold. This value is also called cut-off
        in the literature. If float, the parameter represents a proportion of
        documents, integer absolute counts. This parameter is ignored if vocabulary
        is not None. (default is 1)
    vocabulary : Mapping or iterable, optional
        Either a Mapping (e.g., a dict) where keys are terms and values are indices
        in the feature matrix, or an iterable over terms. If not given, a vocabulary
        is determined from the input documents. (default is None)
    paragraphs : iterable
        an iterable which yields either str, unicode or file objects
    top_n : int (default 20)
        maximum number of top articles (or paragraphs) to retrieve
    verbose : bool, optional
        If true, all of the warnings related to data processing will be printed.
    Attributes
    ----------
    vectorizer : TfidfVectorizer
        
    tfidf_matrix : sparse matrix, [n_samples, n_features]
        Tf-idf-weighted document-term matrix.
    
    """

    def __init__(
        self,
        lowercase=True,
        preprocessor=None,
        tokenizer=None,
        stop_words="english",
        token_pattern=r"(?u)\b\w\w+\b",
        ngram_range=(1, 2),
        max_df=0.85,
        min_df=2,
        vocabulary=None,
        top_n=20,
        verbose=False,
    ):
        self.lowercase = lowercase
        self.preprocessor = preprocessor
        self.tokenizer = tokenizer
        self.stop_words = stop_words
        self.token_pattern = token_pattern
        self.ngram_range = ngram_range
        self.max_df = max_df
        self.min_df = min_df
        self.vocabulary = vocabulary

        vectorizer = TfidfVectorizer(
            lowercase=self.lowercase,
            preprocessor=self.preprocessor,
            tokenizer=self.tokenizer,
            stop_words=self.stop_words,
            token_pattern=self.token_pattern,
            ngram_range=self.ngram_range,
            max_df=self.max_df,
            min_df=self.min_df,
            vocabulary=self.vocabulary,
        )
        super().__init__(vectorizer, top_n, verbose)

    def _fit_vectorizer(self, df, y=None):
        self.tfidf_matrix = self.vectorizer.fit_transform(df["content"])
        return self

    def _compute_scores(self, query):
        question_vector = self.vectorizer.transform([query])
        scores = self.tfidf_matrix.dot(question_vector.T).toarray()
        return scores

class BM25Retriever(BaseRetriever):
    """
    A scikit-learn estimator for BM25Retriever. Trains a matrix based on BM25 statistics
    from a corpus of documents then finds the most N similar documents of a given input
    query by computing the BM25 score for each document based on the query.
    Parameters
    ----------
    lowercase : boolean
        Convert all characters to lowercase before tokenizing. (default is True)
    preprocessor : callable or None
        Override the preprocessing (string transformation) stage while preserving
        the tokenizing and n-grams generation steps. (default is None)
    tokenizer : callable or None
        Override the string tokenization step while preserving the preprocessing
        and n-grams generation steps (default is None)
    stop_words : string {‘english’}, list, or None
        If a string, it is passed to _check_stop_list and the appropriate stop
        list is returned. ‘english’ is currently the only supported string value.
        If a list, that list is assumed to contain stop words, all of which will
        be removed from the resulting tokens.
        If None, no stop words will be used. max_df can be set to a value in the
        range [0.7, 1.0) to automatically detect and filter stop words based on
        intra corpus document frequency of terms.
        (default is None)
    token_pattern : string
        Regular expression denoting what constitutes a “token”. The default regexp
        selects tokens of 2 or more alphanumeric characters (punctuation is completely
        ignored and always treated as a token separator).
    ngram_range : tuple (min_n, max_n)
        The lower and upper boundary of the range of n-values for different n-grams
        to be extracted. All values of n such that min_n <= n <= max_n will be used.
        (default is (1, 1))
    max_df : float in range [0.0, 1.0] or int
        When building the vocabulary ignore terms that have a document frequency strictly
        higher than the given threshold (corpus-specific stop words). If float, the parameter
        represents a proportion of documents, integer absolute counts. This parameter is
        ignored if vocabulary is not None. (default is 1.0)
    min_df : float in range [0.0, 1.0] or int
        When building the vocabulary ignore terms that have a document frequency
        strictly lower than the given threshold. This value is also called cut-off
        in the literature. If float, the parameter represents a proportion of
        documents, integer absolute counts. This parameter is ignored if vocabulary
        is not None. (default is 1)
    vocabulary : Mapping or iterable, optional
        Either a Mapping (e.g., a dict) where keys are terms and values are indices
        in the feature matrix, or an iterable over terms. If not given, a vocabulary
        is determined from the input documents. (default is None)
    paragraphs : iterable
        an iterable which yields either str, unicode or file objects
    top_n : int (default 20)
        maximum number of top articles (or paragraphs) to retrieve
    verbose : bool, optional
        If true, all of the warnings related to data processing will be printed.
    k1 : float, optional (default=2.0)
        term k1 in the BM25 formula
    b : float, optional (default=0.75)
        term b in the BM25 formula
    floor : float or None, optional (default=None)
        floor value for idf terms
    Attributes
    ----------
    vectorizer : BM25Vectorizer
    Examples
    --------
    >>> from cdqa.retriever import BM25Retriever
    >>> retriever = BM25Retriever(ngram_range=(1, 2), max_df=0.85, stop_words='english')
    >>> retriever.fit(df=df)
    >>> best_idx_scores = retriever.predict(query='Since when does the the Excellence Program of BNP Paribas exist?')
    """

    def __init__(
        self,
        lowercase=True,
        preprocessor=None,
        tokenizer=None,
        stop_words="english",
        token_pattern=r"(?u)\b\w\w+\b",
        ngram_range=(1, 2),
        max_df=0.85,
        min_df=2,
        vocabulary=None,
        top_n=20,
        verbose=False,
        k1=2.0,
        b=0.75,
        floor=None,
    ):

        self.lowercase = lowercase
        self.preprocessor = preprocessor
        self.tokenizer = tokenizer
        self.stop_words = stop_words
        self.token_pattern = token_pattern
        self.ngram_range = ngram_range
        self.max_df = max_df
        self.min_df = min_df
        self.vocabulary = vocabulary
        self.k1 = k1
        self.b = b
        self.floor = floor

        vectorizer = BM25Vectorizer(
            lowercase=self.lowercase,
            preprocessor=self.preprocessor,
            tokenizer=self.tokenizer,
            stop_words=self.stop_words,
            token_pattern=self.token_pattern,
            ngram_range=self.ngram_range,
            max_df=self.max_df,
            min_df=self.min_df,
            vocabulary=self.vocabulary,
            k1=self.k1,
            b=self.b,
            floor=self.floor,
        )
        super().__init__(vectorizer, top_n, verbose)

    def _fit_vectorizer(self, df, y=None):
        self.bm25_matrix = self.vectorizer.fit_transform(df["content"])
        return self

    def _compute_scores(self, query):
        question_vector = self.vectorizer.transform([query], is_query=True)
        scores = self.bm25_matrix.dot(question_vector.T).toarray()
        return scores

#from .retriever_sklearn import TfidfRetriever, BM25Retriever

__all__ = ["TfidfRetriever", "BM25Retriever"]

import json
import os
import re
import sys
from tqdm import tqdm
from tika import parser
import pandas as pd
import uuid
import markdown
from pathlib import Path
from html.parser import HTMLParser


def df2squad(df, squad_version="v1.1", output_dir=None, filename=None):
    """
     Converts a pandas dataframe with columns ['title', 'paragraphs'] to a json file with SQuAD format.
     Parameters
    ----------
     df : pandas.DataFrame
         a pandas dataframe with columns ['title', 'paragraphs']
     squad_version : str, optional
         the SQuAD dataset version format (the default is 'v2.0')
     output_dir : str, optional
         Enable export of output (the default is None)
     filename : str, optional
         [description]
    Returns
    -------
    json_data: dict
        A json object with SQuAD format
    
    """

    json_data = {}
    json_data["version"] = squad_version
    json_data["data"] = []

    for idx, row in tqdm(df.iterrows()):
        temp = {"title": row["title"], "paragraphs": []}
        for paragraph in row["paragraphs"]:
            temp["paragraphs"].append({"context": paragraph, "qas": []})
        json_data["data"].append(temp)

    if output_dir:
        with open(os.path.join(output_dir, "{}.json".format(filename)), "w") as outfile:
            json.dump(json_data, outfile)

    return json_data


def generate_squad_examples(question, best_idx_scores, metadata, retrieve_by_doc):
    """
    Creates a SQuAD examples json object for a given for a given question using outputs of retriever and document database.
    Parameters
    ----------
    question : [type]
        [description]
    best_idx_scores : [type]
        [description]
    metadata : [type]
        [description]
    Returns
    -------
    squad_examples: list
        [description]
    
    """

    squad_examples = []

    metadata_sliced = metadata.loc[best_idx_scores.keys()]

    for idx, row in metadata_sliced.iterrows():
        temp = {"title": row["title"], "paragraphs": []}

        if retrieve_by_doc:
            for paragraph in row["paragraphs"]:
                temp["paragraphs"].append(
                    {
                        "context": paragraph,
                        "qas": [
                            {
                                "answers": [],
                                "question": question,
                                "id": str(uuid.uuid4()),
                                "retriever_score": best_idx_scores[idx],
                            }
                        ],
                    }
                )
        else:
            temp["paragraphs"] = [
                {
                    "context": row["content"],
                    "qas": [
                        {
                            "answers": [],
                            "question": question,
                            "id": str(uuid.uuid4()),
                            "retriever_score": best_idx_scores[idx],
                        }
                    ],
                }
            ]

        squad_examples.append(temp)

    return squad_examples


def pdf_converter(directory_path, min_length=200, include_line_breaks=False):
    """
    Function to convert PDFs to Dataframe with columns as title & paragraphs.
    Parameters
    ----------
    min_length : integer
        Minimum character length to be considered as a single paragraph
    include_line_breaks: bool
        To concatenate paragraphs less than min_length to a single paragraph
    Returns
    -------------
    df : Dataframe
    Description
    -----------------
    If include_line_breaks is set to True, paragraphs with character length
    less than min_length (minimum character length of a paragraph) will be
    considered as a line. Lines before or after each paragraph(length greater
    than or equal to min_length) will be concatenated to a single paragraph to
    form the list of paragraphs in Dataframe.
    Else paragraphs are appended directly to form the list.
    """
    list_file = os.listdir(directory_path)
    list_pdf = []
    for file in list_file:
        if file.endswith("pdf"):
            list_pdf.append(file)
    df = pd.DataFrame(columns=["title", "paragraphs"])
    for i, pdf in enumerate(list_pdf):
        try:
            df.loc[i] = [pdf.replace(".pdf",''), None]
            raw = parser.from_file(os.path.join(directory_path, pdf))
            s = raw["content"].strip()
            paragraphs = re.split("\n\n(?=\u2028|[A-Z-0-9])", s)
            list_par = []
            temp_para = ""  # variable that stores paragraphs with length<min_length
            # (considered as a line)
            for p in paragraphs:
                if not p.isspace():  # checking if paragraph is not only spaces
                    if include_line_breaks:  # if True, check length of paragraph
                        if len(p) >= min_length:
                            if temp_para:
                                # if True, append temp_para which holds concatenated
                                # lines to form a paragraph before current paragraph p
                                list_par.append(temp_para.strip())
                                temp_para = (
                                    ""
                                )  # reset temp_para for new lines to be concatenated
                                list_par.append(
                                    p.replace("\n", "")
                                )  # append current paragraph with length>min_length
                            else:
                                list_par.append(p.replace("\n", ""))
                        else:
                            # paragraph p (line) is concatenated to temp_para
                            line = p.replace("\n", " ").strip()
                            temp_para = temp_para + f" {line}"
                    else:
                        # appending paragraph p as is to list_par
                        list_par.append(p.replace("\n", ""))
                else:
                    if temp_para:
                        list_par.append(temp_para.strip())

            df.loc[i, "paragraphs"] = list_par
        except:
            print("Unexpected error:", sys.exc_info()[0])
            print("Unable to process file {}".format(pdf))
    return df

class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return "".join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def md_converter(directory_path):
    """Get all md, convert them to html and create the pandas dataframe with columns ['title', 'paragraphs']"""
    dict_doc = {"title": [], "paragraphs": []}
    for md_file in Path(directory_path).glob("**/*.md"):
        md_file = str(md_file)
        filename = md_file.split("/")[-1]
        try:
            with open(md_file, "r") as f:
                dict_doc["title"].append(filename)
                md_text = f.read()
                html_text = markdown.markdown(md_text)
                html_text_list = list(html_text.split("<p>"))
                for i in range(len(html_text_list)):
                    html_text_list[i] = (
                        strip_tags(html_text_list[i])
                        .replace("\n", " ")
                        .lstrip()
                        .rstrip()
                    )
                clean_text_list = list(filter(None, html_text_list))
                dict_doc["paragraphs"].append(clean_text_list)
        except:
            print("Unexpected error:", sys.exc_info()[0])
            print("Unable to process file {}".format(filename))
    df = pd.DataFrame.from_dict(dict_doc)
    return df

import os
import pandas as pd
import numpy as np


def filter_paragraphs(
    articles,
    drop_empty=True,
    read_threshold=1000,
    public_data=True,
    min_length=50,
    max_length=300,
):
    """
    Cleans the paragraphs and filters them regarding their length
    Parameters
    ----------
    articles : DataFrame of all the articles 
    Returns
    -------
    Cleaned and filtered dataframe
    """

    # Replace and split
    def replace_and_split(paragraphs):
        for paragraph in paragraphs:
            paragraph.replace("'s", " " "s").replace("\\n", "").split("'")
        return paragraphs

    # Select paragraphs with the required size
    def filter_on_size(paragraphs, min_length=min_length, max_length=max_length):
        paragraph_filtered = [
            paragraph.strip()
            for paragraph in paragraphs
            if len(paragraph.split()) >= min_length
            and len(paragraph.split()) <= max_length
        ]
        return paragraph_filtered

    # Cleaning and filtering
    articles["paragraphs"] = articles["paragraphs"].apply(replace_and_split)
    articles["paragraphs"] = articles["paragraphs"].apply(filter_on_size)
    articles["paragraphs"] = articles["paragraphs"].apply(
        lambda x: x if len(x) > 0 else np.nan
    )

    # Read threshold for private dataset
    if not public_data:
        articles = articles.loc[articles["number_of_read"] >= read_threshold]

    # Drop empty articles
    if drop_empty:
        articles = articles.dropna(subset=["paragraphs"]).reset_index(drop=True)

    return articles

import joblib
import warnings

import pandas as pd
import numpy as np

from sklearn.base import BaseEstimator

#from cdqadocumentretriever import TfidfRetriever, BM25Retriever
#from utils.filters import filter_paragraphs 
#from utils.converters import generate_squad_examples
from ast import literal_eval



RETRIEVERS = {"bm25": BM25Retriever, "tfidf": TfidfRetriever}


class DocumentRetriever(BaseEstimator):
    """
    A scikit-learn implementation of the whole cdQA pipeline
    Parameters
    ----------
    retriever: "bm25" or "tfidf"
        The type of retriever
    retrieve_by_doc: bool (default: True). If Retriever will rank by documents
        or by paragraphs.
      """

    def __init__(self, data_path = "./data/job posting dataset.csv", retriever="bm25", retrieve_by_doc=False, **kwargs):

        if retriever not in RETRIEVERS:
            raise ValueError(
                "You provided a type of retriever that is not supported. "
                + "Please provide a retriver in the following list: "
                + str(list(RETRIEVERS.keys()))
            )

        retriever_class = RETRIEVERS[retriever]

        kwargs_retriever = {
            key: value
            for key, value in kwargs.items()
            if key in retriever_class.__init__.__code__.co_varnames
        }

        self.retriever = retriever_class(**kwargs_retriever)

        self.data_path = data_path

        self.retrieve_by_doc = retrieve_by_doc

    def fit_retriever(self):
        """ Fit the QAPipeline retriever to a list of documents in a dataframe.
        Parameters
        ----------
        df: pandas.Dataframe
            Dataframe with the following columns: "title", "paragraphs"
        """

        df = pd.read_csv( self.data_path,
                          converters={"paragraphs": literal_eval},
                        )

        df = filter_paragraphs(df)

        if self.retrieve_by_doc:
            self.metadata = df
            self.metadata["content"] = self.metadata["paragraphs"].apply(
                lambda x: " ".join(x)
            )
        else:
            self.metadata = self._expand_paragraphs(df)

        self.retriever.fit(self.metadata)

        return self

    def get_best_indexes(
        self,
        query: str = None
    ):
        best_idx_scores = self.retriever.predict(query)
        return best_idx_scores

    @staticmethod
    def _expand_paragraphs(df):
        
        lst_col = "paragraphs"
        df = pd.DataFrame(
            {
                col: np.repeat(df[col].values, df[lst_col].str.len())
                for col in df.columns.drop(lst_col)
            }
        ).assign(**{lst_col: np.concatenate(df[lst_col].values)})[df.columns]
        df["content"] = df["paragraphs"]
        return df.drop("paragraphs", axis=1)

    def get_most_relevant_paragraph(self, query):
        try:
            self.fit_retriever()
        except:
            print('error -> ', query)
            
            return {'title': '<title></title>', 'paragraphs': []}
        bestIndexes = self.get_best_indexes(query)
        squad_examples = generate_squad_examples(
            question=query,
            best_idx_scores=bestIndexes,
            metadata=self.metadata,
            retrieve_by_doc=self.retrieve_by_doc,
        )

        return squad_examples[0]

import torch
from transformers import AlbertTokenizer, AlbertForQuestionAnswering
from transformers import BertTokenizer, BertForQuestionAnswering
#from transformers import AutoModel
#model = AutoModel.from_pretrained("bert-base-cased")

#model_path should be "path_to_your_model"
model_path ="bert-base-cased"
#model_path ="distilbert-base-uncased"

class QAModelLoader:

    def __init__(self, model_path = model_path):
        self.tokenizer = BertTokenizer.from_pretrained(model_path, return_dict=False)
        self.model = BertForQuestionAnswering.from_pretrained(model_path, return_dict=False)

    def answer(self, question, text,return_dict=False):
        #input_dict = self.tokenizer.encode_plus(question, text, return_tensors='pt', max_length=1024)
        input_dict = self.tokenizer.encode_plus(question, text, return_tensors='pt')
        input_ids = input_dict["input_ids"].tolist()
        start_scores, end_scores = self.model(**input_dict)

        start = torch.argmax(start_scores, dim=1)
        end = torch.argmax(end_scores, dim=1)

        all_tokens = self.tokenizer.convert_ids_to_tokens(input_ids[0])
        answer = ''.join(all_tokens[start: end + 1]).replace('▁', ' ').strip()
        answer = answer.replace('[SEP]', '')
        #return answer if answer != '[CLS]' and len(answer) != 0 else 'could not find an answer'
        return answer if answer != '[CLS]' and len(answer) != 0 else text

from flask import Flask, request, jsonify
from flask_cors import CORS
import os
from simpletransformers.question_answering import QuestionAnsweringModel
from googlesearch import search
import pandas as pd
#from documentretriever import DocumentRetriever
#from qamodelloader import QAModelLoader
#from customdocumentretriever.customgooglesearchengine import CustomGoogleSearchEngine

# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

model = QAModelLoader()
data_path = "/content/sample_data/collecteddatafromgoogle.csv"
# df=pd.read_csv(data_path)

"""### **Search Answer**"""

def search_answer(question):

    search_engine = CustomGoogleSearchEngine(question)
    doc_trunk = search_engine.buildDocumentTrunk()
    documentRetriever = DocumentRetriever(data_path = doc_trunk)
    squad_examples = documentRetriever.get_most_relevant_paragraph(question)
    if len(squad_examples['paragraphs']) > 0:
        context = squad_examples['paragraphs'][0]['context']
    else:
        return ''
    answer = model.answer(question, context)

    return answer


# In[48]:


job_detail = {'job_detail':'Java Developer', 'job_description':"\"<div id=\\\"jobDescriptionText\\\" class=\\\"jobsearch-jobDescriptionText\\\"><div>\\n<div><b>Job Overview:</b></div>\\n<div></div>\\n<div>The Macy’s Pricing Domain will build enterprise, integrated, scalable pricing solutions that support Markdown and Point of Sales Optimization along with competitive and dynamic pricing across all channels (Stores, .COM and Mobile). Members of our team will utilize cutting edge technologies (Cloud, Java, Microservices) and data techniques to discover solutions and build from the ground up, driving results that produce tangible impact. Teams will complete work utilizing lean delivery methodologies, and will partner closely with Product Management, Architecture, and Pricing Strategy teams. This team will focus on enabling a new cloud solution, building new database structures/schema and a pricing rules engine, as well as create all enterprise pricing common services to provide accurate pricing data and history.<br>\\n</div>\\n<div></div>\\n<div>Technical Lead, Software Engineering at Macy’s Technology reports to the Manager, Software Engineering, and plays a key role in leading the software engineering technical direction and development of enterprise solutions. As a lead engineer, you serve as the technical anchor for the engineering team that supports a product. You create, own and are responsible for the application architecture that best serves the product in its functional and non-functional needs. You identify and drive architectural changes to accelerate feature development or improve the quality of service (or both). You have deep and broad engineering skills and are capable of standing up enterprise architecture in its whole on your own, but you choose to influence a wider team by acting as a “force multiplier”. Job duties may change at any time due to business needs. Perform other duties as assigned.</div>\\n<br>\\n<div></div>\\n<div><b>Essential Functions:</b></div>\\n<ul>\\n<li><div>Design and deliver robust, scalable, and secure distributed software systems (scalability, security, stability, availability).</div></li>\\n<li>\\nWrite code to implement microservices that deploy to cloud infrastructure and scale to millions of requests per day.</li>\\n<li>\\nPartners with team to drive resolution and process improvements to further minimize impact to our production environment and support business partners. Documents all key processes to further drive performance.\\n</li>\\n<li>Create/review technical design documentation and Provide leadership including constructive code reviews and design guidance to team members</li>\\n<li>\\nWork collaboratively in a group of passionate developers in a high energy, fast paced and outcome-oriented environment.</li>\\n<li>\\nLead a technical team by example and foster growth in junior team members.</li>\\n<li>\\nRegular, dependable attendance &amp; punctuality.</li>\\n</ul>\\n<div>\\n<b>Qualifications</b><b>:</b>\\n</div>\\n<div></div>\\n<div>\\n</div>\\n<div><b>Education/Experience:\\n</b></div>\\n<ul>\\n<li><div>Bachelor's Degree in Computer Science and/or Engineering preferred and 6+ years of related experience or an equivalent combination of education and experience.\\n</div></li>\\n<li>Overall 6+ years of experience in the software designing and developing web and enterprise applications using Java/J2EE and high-volume enterprise systems as well as experienced in open-source web and data technologies (Spring Boot, Microservices, NoSQL/SQL (Cassandra, MongoDB, DynamoDB, SQL Server and Oracle)).</li>\\n<li>\\n3+ years of experience in Building cloud-native applications in private &amp; public cloud (GCP preferred).</li>\\n<li>\\nExperience with open source technology technologies in a high volume distributed environment.</li>\\n<li>\\nExperience working in a DevOps-oriented culture that prioritizes continuous integration &amp; continuous deployment.</li>\\n<li>\\nExperience in multi-channel retail technologies preferred.</li>\\n</ul>\\n<div><b>Communication Skills:\\n</b></div>\\n<ul>\\n<li><div>Demonstrated ability to effectively share technical information and train and mentor less experienced or knowledgeable team members.</div></li>\\n<li><div>\\nAdvanced presentation (written and verbal) communication skills. Ability to effectively communicate technical issues and solutions to all levels of Product Engineering and Product Management.</div></li>\\n</ul>\\n<div><b>Mathematical Skills:</b></div>\\n<ul><li><div>Basic math functions such as addition, subtraction, multiplication, division, and analytical skills.</div></li></ul>\\n<div><b>Reasoning Ability:</b></div>\\n<ul><li><div>Strong analytical and problem-solving skills.</div></li></ul>\\n<div><b>Physical Demands:</b></div>\\n<ul>\\n<li><div>This position involves regular walking, standing, sitting for extended periods of time, hearing, and talking.\\n</div></li>\\n<li><div>May occasionally involve stooping, kneeling, or crouching.\\n</div></li>\\n<li><div>May involve close vision, color vision, depth perception, focus adjustment, and viewing computer monitor for extended periods of time.\\n</div></li>\\n<li><div>Involves manual dexterity for using keyboard, mouse, and other office equipment.\\n</div></li>\\n<li><div>May involve moving or lifting items under 10 pounds.</div></li>\\n</ul>\\n<div><b>\\nOther Skills:</b></div>\\n<ul>\\n<li><div>\\nUnderstanding of full life cycle software development with Agile and Lean Startup methodologies.</div></li>\\n<li><div>\\nKnowledge of Unit Testing best practices and Continuous Integration processes.</div></li>\\n<li><div>\\nAbility to effectively learn and use new programming concepts, tools and methodology to support the needs of the business.</div></li>\\n</ul>\\n<div><b>Work Hours:</b></div>\\n<ul><li><div>Ability to work a flexible schedule based on department and company needs.</div></li></ul>\\n<div><b>Company Profile:</b></div>\\n<div></div>\\n<div>Macy’s Inc. is one of the nation’s premier retailers. With fiscal 2016 sales of $25.778 billion and approximately 140,000 employees, the company operates more than 700 department stores under the nameplates Macy’s and Bloomingdale’s, and approximately 125 specialty stores that include Bloomingdale’s The Outlet, Bluemercury and Macy’s Backstage. Macy’s, Inc. operates stores in 45 states, the District of Columbia, Guam and Puerto Rico, as well as macys.com, bloomingdales.com and bluemercury.com. Bloomingdale’s stores in Dubai and Kuwait are operated by Al Tayer Group LLC under license agreements. Macy’s, Inc. has corporate offices in Cincinnati, Ohio and New York, New York.</div>\\n<br>\\n<div></div>\\n<div><b>This job description is not all inclusive. Macy’s Inc. reserves the right to amend this job description at any time. Macy's Inc. is an Equal Opportunity Employer, committed to a diverse and inclusive work environment.</b></div>\\n</div></div>\""}


# In[49]:


import json


# In[53]:


sections_list = []

# extract sections
extract_section(json.loads(job_detail['job_description']), sections_list)

# Create dataframe from sections extracted
df_sections = pd.DataFrame(sections_list)

# remove nan rows
df_sections = df_sections[df_sections['content'].notna()]

# Add content_summerized column to the data frame
df_sections['content_summerized'] = df_sections.apply(lambda x: extract_intent_skills_based_on_category(x), axis=1)


# Search questions
topic_questions = search_for(df_sections)

# Create data frame from questions searched
df_with_questions_summerized_content = pd.DataFrame(topic_questions)

display(df_with_questions_summerized_content.head())


# In[55]:


search_answer("'What Machine Learning?'")


# In[ ]:


# Add answer column to the data frame
df_with_questions_summerized_content['answer'] = df_with_questions_summerized_content.apply(
    lambda x: search_answer("'"+x['questions']+"'"), axis=1)
df_with_questions_summerized_content.head()


# In[ ]:


import uvicorn
from fastapi import FastAPI

app = FastAPI()

def run():
    uvicorn.run(app)


# In[ ]:



from multiprocessing import Process
from wait4it import wait_for

_api_process = None

def start_api():
    """Stop the API if running; Start the API; Wait until API (port) is available (reachable)"""
    global _api_process
    if _api_process:
        _api_process.terminate()
        _api_process.join()
    
    _api_process = Process(target=run, daemon=True)
    _api_process.start()
    wait_for(port=8000)

def delete_route(method: str, path: str):
    """Delete the given route from the API. This must be called on cells that re-define a route"""
    [app.routes.remove(route) for route in app.routes if method in route.methods and route.path == path]


# In[ ]:


@app.post("/qna/")
def get_root():
    
    sections_list = []

    # extract sections
    extract_section(json.loads(job_detail['job_description']), sections_list)

    # Create dataframe from sections extracted
    df_sections = pd.DataFrame(sections_list)

    # remove nan rows
    df_sections = df_sections[df_sections['content'].notna()]

    # Add content_summerized column to the data frame
    df_sections['content_summerized'] = df_sections.apply(lambda x: extract_intent_skills_based_on_category(x), axis=1)


    # Search questions
    topic_questions = searh_for(df_sections)

    # Create data frame from questions searched
    df_with_questions_summerized_content = pd.DataFrame(topic_questions)

    display(df_with_questions_summerized_content.head())

    # Add answer column to the data frame
    df_with_questions_summerized_content['answer'] = df_with_questions_summerized_content.apply(
        lambda x: search_answer("'"+x['questions']+"'"), axis=1)

    # Add answer column to the data frame
    df_with_questions_summerized_content['answer'] = df_with_questions_summerized_content.apply(
        lambda x: search_answer(x['questions']), axis=1)

    df_with_questions_summerized_content.head()
    return {"Hello": "World"}

start_api()


# In[ ]:




