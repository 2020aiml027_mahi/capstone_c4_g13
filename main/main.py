import fastapi
import uvicorn

from qna_maker.api import qna_api

api = fastapi.FastAPI()


def configure():
    api.include_router(qna_api.router)


configure()
if __name__ == '__main__':
    uvicorn.run(api)
