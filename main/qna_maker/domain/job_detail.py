from typing import Optional

from pydantic import BaseModel


class JobDetail(BaseModel):
    job_title: str
    job_description: str
