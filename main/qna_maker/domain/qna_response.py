from pydantic import BaseModel


class QnAResponse(BaseModel):
    qna: list
