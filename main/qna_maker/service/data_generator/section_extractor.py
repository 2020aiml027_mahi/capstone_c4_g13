from qna_maker.utils.contants import skills_keywords, roles_responsibility_keywords
from nltk.stem.porter import PorterStemmer
from bs4 import BeautifulSoup
import re

stemmed_skills_keywords = [PorterStemmer().stem(word) for word in skills_keywords]
stemmed_roles_responsibility_keywords = [PorterStemmer().stem(word) for word in roles_responsibility_keywords]


def can_we_consider_this_section_header(section_header):
    if any(s in section_header.lower() for s in stemmed_skills_keywords):
        return {'consider': True, 'category': 'Skills'}
    if any(s in section_header.lower() for s in stemmed_roles_responsibility_keywords):
        return {'consider': True, 'category': 'Responsibility'}
    return {'consider': False, 'category': 'Other'}


def extract_sections(df, idx, sections_list):
    extract_section(df['job_description'][idx], sections_list)


def extract_section(job_description, sections_list):
    soup = BeautifulSoup(job_description, 'lxml')

    job_description = soup.findAll('div', {'class': 'jobsearch-jobDescriptionText'})[0].text
    all_section_headers = soup.find_all('b')

    for i in range(0, len(all_section_headers)):
        section_head = {}

        section_consideration_result = can_we_consider_this_section_header(all_section_headers[i].text)
        if not section_consideration_result['consider']:
            continue

        section_head['category'] = section_consideration_result['category']
        section_head['title'] = all_section_headers[i].text

        start_idx = job_description.index(all_section_headers[i].text) + len(all_section_headers[i].text)
        end_idx = len(job_description) if len(all_section_headers) == i + 1 else job_description.index(
            all_section_headers[i + 1].text)

        content = job_description[start_idx:end_idx]
        content = re.sub(r'\n+', '. ', content).strip()
        section_head['content'] = content

        sections_list.append(section_head)
    return sections_list
