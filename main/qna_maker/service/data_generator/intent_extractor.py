from sparknlp.base import *
from sparknlp.annotator import *
import sparknlp
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
import nltk

from qna_maker.utils.contants import all_skills


def get_ann_pipeline():
    document_assembler = DocumentAssembler() \
        .setInputCol("text") \
        .setOutputCol('document')

    sentence = SentenceDetector() \
        .setInputCols(['document']) \
        .setOutputCol('sentence') \
        .setCustomBounds(['\n'])

    tokenizer = Tokenizer() \
        .setInputCols(["sentence"]) \
        .setOutputCol("token")

    pos = PerceptronModel.pretrained() \
        .setInputCols(["sentence", "token"]) \
        .setOutputCol("pos")

    embeddings = WordEmbeddingsModel.pretrained() \
        .setInputCols(["sentence", "token"]) \
        .setOutputCol("embeddings")

    ner_model = NerDLModel.pretrained() \
        .setInputCols(["sentence", "token", "embeddings"]) \
        .setOutputCol("ner")

    ner_converter = NerConverter() \
        .setInputCols(["sentence", "token", "ner"]) \
        .setOutputCol("ner_chunk")

    ner_pipeline = Pipeline(
        stages=[
            document_assembler,
            sentence,
            tokenizer,
            pos,
            embeddings,
            ner_model,
            ner_converter
        ]
    )

    empty_data = spark.createDataFrame([[""]]).toDF("text")

    ner_pipelineFit = ner_pipeline.fit(empty_data)

    ner_lp_pipeline = LightPipeline(ner_pipelineFit)

    print("Spark NLP NER lightpipeline is created")

    return ner_lp_pipeline


def extract_nouns_as_skills(content):
    skills = []
    for sent in sent_tokenize(content):
        tokens = nltk.word_tokenize(sent)
        tag = nltk.pos_tag(tokens)
        result = cp.parse(tag)
        extract_nouns(result, skills)
    return skills


def extract_nouns(pos_tagged_result, skills):
    for tag in pos_tagged_result:
        if type(tag) is tuple and (tag[1] == 'NNP' or tag[1] == 'NN') and len(tag[0]) > 1 and tag[0] not in all_skills:
            skills.append(tag[0])
        elif type(tag) is nltk.tree.Tree:
            skill = ''
            for t in tag:
                if type(t) is tuple and (t[1] == 'NNP' or t[1] == 'NN') and len(t[0]) > 1:
                    skill = skill + ' ' + t[0]
            if len(skill) > 1 and skill.strip() not in all_skills:
                skills.append(skill.strip())


# def extranct_skills_from_content():
#     text = df_skills.content.values[i]
#     skills = extract_nouns_as_skills(text)
#     print(skills)

def extract_intent_skills_based_on_category(row):
    if row['category'] == 'Skills':
        return extract_skills(row['content'])
    elif row['category'] == 'Responsibility':
        return extract_intent(row['content'])


def extract_intent(content):
    intent_text = ''
    parsed = get_ann_pipeline().annotate(content)
    for token, pos, ner in zip(parsed['token'], parsed['pos'], parsed['ner']):
        if ((pos == 'NP') or (pos == 'NNP') or (pos[0] == 'V')) or (ner != 'O'):
            intent_text = intent_text + token + " "
    return intent_text


def extract_skills(content):
    skills = []
    for skill in word_tokenize(content):
        if skill in all_skills:
            skills.append(skill) if skill not in skills else skills
    return skills


spark = sparknlp.start()
grammar = "NN: {<NNP>+|<NN>+}"
cp = nltk.RegexpParser(grammar)
