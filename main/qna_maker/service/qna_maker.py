import pandas as pd
import json

from qna_maker.domain.job_detail import JobDetail
from qna_maker.domain.qna_response import QnAResponse
from qna_maker.domain.question_answer import QuestionAnswer
from qna_maker.service.answer_finder import search_answer
from qna_maker.service.data_generator.intent_extractor import extract_intent_skills_based_on_category
from qna_maker.service.data_generator.section_extractor import extract_section
from qna_maker.service.questions_finder import search_questions_from_given_df


def question_and_answer_maker(job_detail: JobDetail):
    sections_list = []

    # extract sections
    extract_section(json.loads(job_detail.job_description), sections_list)

    # Create dataframe from sections extracted
    df_sections = pd.DataFrame(sections_list)

    # remove nan rows
    df_sections = df_sections[df_sections['content'].notna()]

    # Add content_summerized column to the data frame
    df_sections['content_summerized'] = df_sections.apply(lambda x: extract_intent_skills_based_on_category(x), axis=1)

    # Search questions
    topic_questions = search_questions_from_given_df(df_sections)

    # Create data frame from questions searched
    df_with_questions_summerized_content = pd.DataFrame(topic_questions)

    # Add answer column to the data frame
    df_with_questions_summerized_content['answer'] = df_with_questions_summerized_content.apply(
        lambda x: search_answer("'" + x['questions'] + "'"), axis=1)

    # Build Response object
    all_questions_with_answers = list()
    for index, row in df_with_questions_summerized_content.iterrows():
        all_questions_with_answers.append(QuestionAnswer(question=row['questions'], answer=row['answer']))
        response = QnAResponse(qna=list())
        response.qna = all_questions_with_answers
    return response
