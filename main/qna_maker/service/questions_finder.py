from string import digits
import selenium
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

from googlesearch import search


def search_questions_for(driver, search_keyword, job_title, max_questions=10):
    search_text = 'top ' + search_keyword + ' interview questions' + ' for ' + job_title
    links = search(search_text, num_results=10)
    raw_questions = []

    for link in links:
        if link.startswith('http'):
            if len(raw_questions) == max_questions:
                return raw_questions
            try:
                extract_raw_questions(driver, link, raw_questions, max_questions)
            except:
                print('skipping -> ' + link)
    return raw_questions


symbols = "!\"#$%&()*+-./:;<=>?@[\]^_`{|}~\n"


def pre_process_question(question):
    question = question[0:question.index('?') + 1]  # extract only question portion
    question = ' '.join(question.split())  # remove multiple spaces
    question = question.lstrip(digits)  # remove trailing digits
    question = question.lstrip(symbols)  # remove trailing special chars
    question = question.strip()
    return question


def extract_raw_questions(driver, web_page_link, raw_questions, max_questions):
    try:
        driver.get(web_page_link)
        text_elements_containing_question_mark = driver.find_elements(By.XPATH, "//*[contains(text(), '?')]")
        for question_ele in text_elements_containing_question_mark:
            question = question_ele.text
            if len(question) != 0:
                if len(raw_questions) == max_questions:
                    return
                question = pre_process_question(question)
                raw_questions.append(question)
    except:
        print('exception occurred')


# chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('--headless')
# chrome_options.add_argument('--no-sandbox')
# chrome_options.add_argument('--disable-dev-shm-usage')
# wd = webdriver.Chrome('chromedriver', chrome_options=chrome_options)
opts = webdriver.ChromeOptions()
opts.headless = True
wd = webdriver.Chrome(ChromeDriverManager().install(), options=opts)


def search_questions_from_given_df(df_sections):
    topic_questions = []
    for index, row in df_sections.iterrows():
        t = row['content_summerized']
        job_title = row['title']
        if type(t) is list:
            for topic in t:
                questions = search_questions_for(wd, topic, job_title, 10)
                for question in questions:
                    topic_questions.append({'topic': t, 'questions': question})
        else:
            questions = search_questions_for(wd, t, job_title, 10)
            for question in questions:
                topic_questions.append({'topic': t, 'questions': question})
    return topic_questions


def search_questions_for_job(topic, job_title):
    topic_questions = []
    questions = search_questions_for(wd, topic, job_title, 10)
    return topic_questions


def stop_browser():
    wd.quit()
