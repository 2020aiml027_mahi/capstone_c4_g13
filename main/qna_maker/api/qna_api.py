import fastapi

from qna_maker.domain.job_detail import JobDetail
from qna_maker.domain.qna_response import QnAResponse
from qna_maker.service.qna_maker import question_and_answer_maker

router = fastapi.APIRouter()


@router.get('/api/test')
async def test():
    return 'Hello World!!'


@router.post('/api/generate/qna', response_model=QnAResponse)
async def generate_qna(job_detail: JobDetail):
    return question_and_answer_maker(job_detail)
