import nltk
import re
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem.porter import PorterStemmer


def remove_features_having_no_values(df):
    missing_data = df.isnull()
    df.drop(['uniq_id', 'crawl_timestamp'], axis=1, inplace=True)
    for column in missing_data.columns.values.tolist():
        if missing_data[column].value_counts().get(True) == df.shape[0]:
            print("removing ", column)
            df.drop([column], axis=1, inplace=True)


# StopWord Removal
stop_words = set(stopwords.words('english'))


def remove_stop_words(sentence):
    word_tokens = word_tokenize(sentence)

    filtered_sentence = []
    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w)

    return " ".join(filtered_sentence)


# Punctuation Removal
symbols = "!\"#$%&()*+-./:;<=>?@[\]^_`{|}~\n"


def remove_punctuation(sentence):
    word_tokens = word_tokenize(sentence)

    filtered_sentence = []
    for w in word_tokens:
        if w not in symbols:
            filtered_sentence.append(w)

    return " ".join(filtered_sentence)


def remove_special_characters(text):
    pattern = r'[^a-zA-z0-9\s]'
    text = re.sub(pattern, '', text)
    text = text.lower()
    return text


def basic_preprocess_text(text):
    # Remove stopWords
    text = remove_stop_words(text)

    # Remove punctutation
    #     text = remove_punctuation(text)

    # Removal special characters
    #     text = re.sub("(\\W)+"," ",text)
    text = re.sub("\n+", ". ", text)
    return text


def stemmer():
    return PorterStemmer()
